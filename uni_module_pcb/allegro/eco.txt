|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   1  |
|------------------------------------------------------------------------------|
|  c:/works/rqc_pulse_counter/uni_module_pcb/allegro/unimodule.brd             |
|                                                    Wed Apr 14 15:01:43 2021  |
|------------------------------------------------------------------------------|
| NET CHANGES                                                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   net  name |  type of change |    pin_id     |   x   |   y   |   to  net    |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
  1V0_TEST     pins MOVED FROM this net (to net name listed on right)
                                 X1.159           52.300 -28.500
                                                                           TD1-
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  1V5_TEST     pins MOVED FROM this net (to net name listed on right)
                                 X1.167           54.700 -28.500
                                                                           TD3-
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  1V8_TEST     pins MOVED FROM this net (to net name listed on right)
                                 X1.163           53.500 -28.500
                                                                           TD2-
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  DONE         pins MOVED FROM this net (to net name listed on right)
                                 X1.171           55.900 -28.500
                                                                           TD4-
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  GND          pins DELETED FROM this net (pin now not on any net)
                                 X1.50            16.600 -28.500
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
               pins MOVED FROM this net (to net name listed on right)
                                 X1.34            11.800 -28.500
                                                                       1V0_TEST
                                 X1.38            13.000 -28.500
                                                                       1V8_TEST
                                 X1.42            14.200 -28.500
                                                                       1V5_TEST
                                 X1.46            15.400 -28.500
                                                                           DONE
                                 X1.157           51.700 -28.500
                                                                           TD1+
                                 X1.161           52.900 -28.500
                                                                           TD2+
                                 X1.165           54.100 -28.500
                                                                           TD3+
                                 X1.169           55.300 -28.500
                                                                           TD4+
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  NP3          pins DELETED FROM this net (pin now not on any net)
                                 X1.54            17.800 -28.500
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
               pins ADDED TO this existing net (pins not previously on any net)
                                 X1.202           64.900 -28.500
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   2  |
|------------------------------------------------------------------------------|
|  c:/works/rqc_pulse_counter/uni_module_pcb/allegro/unimodule.brd             |
|                                                    Wed Apr 14 15:01:43 2021  |
|------------------------------------------------------------------------------|
| NET CHANGES                                                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   net  name |  type of change |    pin_id     |   x   |   y   |   to  net    |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|------------------------------------------------------------------------------|
|       Nets changed          :      15                                        |
|                                                                              |
|   Total ECO changes reported:      15                                        |
|------------------------------------------------------------------------------|
